clean:
	find . -name "*~" -delete

deploy:
	git archive master | tar -x -C /home/hubert/public_html/backbone_test -f -

test:
	cp -fr * ~/public_html/backbone_test/
