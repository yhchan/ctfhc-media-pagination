(function ( views ) {
    views.AppView = Backbone.View.extend({
        el: null,

        initialize : function () {
            var tags = this.collection;

            tags.on('add', this.addOne, this);
            tags.on('reset', this.addAll, this);
            tags.on('all', this.render, this);

            tags.fetch({
                success: function(){
                    tags.pager();
                },
                silent:true
            });
        },

        addAll : function () {
            this.$el.empty();
            this.collection.each (this.addOne, this);
        },

        addOne : function ( item ) {
            var view = new views.ResultView({model:item});
            this.$el.append(view.render().el);
        },

        render: function(){
        }
    });
})( app.views );
