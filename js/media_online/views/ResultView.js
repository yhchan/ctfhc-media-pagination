( function ( views ){

    views.ResultView = Backbone.View.extend({
        tagName : 'tr',
        template: _.template($('#resultItemTemplate').html()),

        events: {
            'mouseover': 'onMouseOver',
            'mouseout': 'onMouseOut'
        },

        initialize: function() {
            this.model.bind('change', this.render, this);
            this.model.bind('destroy', this.remove, this);
        },

        render : function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        onMouseOver: function() {
            this.$el.css('background-color', '#ECFBD4');
            this.$el.css('color', '#990000');
        },

        onMouseOut: function() {
            this.$el.css('background-color', '#FFFFFF');
            this.$el.css('color', '#333333');
        }
    });

})( app.views );
