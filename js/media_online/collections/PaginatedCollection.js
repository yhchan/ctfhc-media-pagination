(function (collections, model, paginator) {
    collections.PaginatedCollection = paginator.clientPager.extend({
        model: model,

        paginator_core: {
            type: 'GET',
            dataType: 'jsonp',
            url: 'media.php'
        },
        paginator_ui: {
            firstPage: 1,
            currentPage: 1,
            perPage: 5,
            totalPages: 10
        },
        parse: function (response) {
            return response;
        }
    });
 
}) (app.collections, app.models.MediaItem, Backbone.Paginator);
