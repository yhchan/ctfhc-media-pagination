(function ( models ) {
    models.MediaItem = Backbone.Model.extend({
        defaults: {
            date: '',
            title: '',
            speaker: '',
            audio: '',
            video: ''
        }
    });
}) (app.models);
