(function () {
    window.app = {};
    app.collections = {};
    app.models = {};
    app.views = {};
    app.mixins = {};

    $(function(){
        /* 
         * options.id
         * options.server_api
        */
        var renderer = function(options) {

            var content = $(options.id);
            var tbody = $('tbody', content);

            // Clear content
            tbody.empty();

            app.collections.paginatedItems = new app.collections.PaginatedCollection();
            app.collections.paginatedItems.server_api = options.server_api;
            app.views.app = new app.views.AppView(
                {
                    collection: app.collections.paginatedItems,
                    el: tbody
                }
            );
            app.views.pagination = new app.views.PaginationView(
                {
                    collection: app.collections.paginatedItems,
                    paginationContainer: $('.pagination', content)
                }
            );
        };

        var options = [
            {
                'id': '#sermon-A0',
                'server_api': {'id': 'sermon-A0'}
            },
            {
                'id': '#sermon-A1',
                'server_api': {'id': 'sermon-A1'}
            },
            {
                'id': '#sermon-A2',
                'server_api': {'id': 'sermon-A2'}
            }
        ];

        _.each(options, renderer);

    });
})();
