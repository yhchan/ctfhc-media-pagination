<?php


function DOMinnerHTML($element) 
{ 
    $innerHTML = ""; 
    $children = $element->childNodes; 
    foreach ($children as $child) { 
        $tmp_dom = new DOMDocument(); 
        $tmp_dom->appendChild($tmp_dom->importNode($child, true)); 
        $innerHTML .= trim($tmp_dom->saveHTML()); 
    } 
    return $innerHTML; 
} 

function parseTable($nodeList) {
    $field = array('date', 'title', 'speaker', 'audio', 'video');
    $output = array();

    for ($i = 0; $i < $nodeList->length; $i++) {
        $node = $nodeList->item($i);
        $childNodes = $node->childNodes;

        $values = array();
        for ($j = 0; $j < $childNodes->length; $j++) {
            $item = $childNodes->item($j);
            if ($item->nodeType == XML_ELEMENT_NODE) {
                $values[] = html_entity_decode(
                    DOMinnerHTML($item), ENT_COMPAT | ENT_XHTML, 'UTF-8'
                );
            }
        }
        $row = array();
        for ($j = 0; $j < count($field); $j++) {
            $key = $field[$j];
            $val = $values[$j];
            $row[$key] = $val;
        }
        $output[] = $row;
    }
    return $output;
}

function main() {
    $target = __DIR__ . '/media_online.html';

    $doc = new DOMDocument;
    $doc->loadHTMLFile($target);

    $id = $_GET['id'];
    $output = array();

    // Only process id in ALLOWED_ID
    $ALLOWED_ID = array('sermon-A0', 'sermon-A1', 'sermon-A2');
    if (in_array($id, $ALLOWED_ID)) {
        $xpath = new DOMXpath($doc);
        $xpathtpl = "//table[@id='%s']/tbody/tr";

        $xpathstr = sprintf($xpathtpl, $id);
        $elements = $xpath->query($xpathstr);

        $row = parseTable($elements);
        $output = $row;
    }

    $jsonOutput = json_encode($output);
    header('Content-type: application/json');
    if (isset($_GET['callback'])) {
        echo $_GET['callback'] . '('.$jsonOutput.')';
    } else {
        echo $jsonOutput; 
    }
}

main();
